//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image,BackHandler } from 'react-native';
import { Router, Scene, Stack, Drawer } from 'react-native-router-flux';
import { Actions } from 'react-native-router-flux';
import Login from './src/component/Login'
import otpscreen from './src/component/otpscreen'
import SignUp from './src/component/SignUp'
import Home from './src/component/Home'
import PolicyDetails from './src/component/PolicyDetails'
import Upload from './src/component/Upload'
import UploadCar from './src/component/UploadCar'
import Loading from './src/component/Loading'
import Result from './src/component/Result'
import SplashScreen from 'react-native-splash-screen';

export default class Root extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentWillMount(){
        SplashScreen.hide()    
        console.disableYellowBox = true;
    }
    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', this.hardwareBackPress.bind(this));

    }



    hardwareBackPress() {
        Actions.drawerClose()   
        const scene = Actions.currentScene;
        if (scene == 'Login' ) {
            Actions.UserAccountList()

            return true;
        }
        else if (scene == 'About' ) {
            Actions.UserAccountList()

            return true;
        }
        else if (scene == 'News' ) {
            Actions.UserAccountList()

            return true;
        }
        else {
            // Actions.Home();
            return true;
        }
    }

    render(){

    
    return (
        <Router>
                <Scene
                    key="Scene_1"
                >
                    <Scene key="Login" drawer={false} component={Login} type='reset' hideNavBar={true} initial/>
                    <Scene key="otpscreen" drawer={false} component={otpscreen} type='reset' hideNavBar={true} />
                    <Scene key="SignUp" drawer={false} component={SignUp} type='reset' hideNavBar={true} />
                    <Scene key="Home" drawer={false} component={Home} type='reset' hideNavBar={true} />
                    <Scene key="PolicyDetails" drawer={false} component={PolicyDetails} type='reset' hideNavBar={true} />
                    <Scene key="Upload" drawer={false} component={Upload} type='reset' hideNavBar={true} />
                    <Scene key="UploadCar" drawer={false} component={UploadCar} type='reset' hideNavBar={true} />
                    <Scene key="Loading" drawer={false} component={Loading} type='reset' hideNavBar={true} />
                    <Scene key="Result" drawer={false} component={Result} type='reset' hideNavBar={true} />


                </Scene>
        </Router>


    );
}
}

