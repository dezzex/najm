import React, { Component } from 'react';
import { View, Text, SafeAreaView,ImageBackground,Image,TextInput } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import StarRating from 'react-native-star-rating';
import { Actions } from 'react-native-router-flux';

export default class PolicyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <SafeAreaView style={{width:width(99),height:height(99),alignItems:"center",justifyContent:"center"}}>
      <ImageBackground source={require("../assets/mainbg2.png")} resizeMode="contain" style={{width:width(100),height:height(99),alignItems:"center",justifyContent:"center"}}>
     
      <View style={{width:width(85),height:height(10),alignItems:"center",justifyContent:"center"}}>
      <Text style={{fontSize:28,fontFamily:"Helvetica Neue",color:"#606060",fontWeight:"bold"}}>
      Enter your policy number
      </Text>
   </View>
   <View style={{width:width(85),height:height(60),alignItems:"center",justifyContent:"center"}}>
   <TextInput style={{borderRadius:24,borderBottomColor:"#383838",width:width(60),height:height(6),
    color:"#000",fontSize:18}}
    placeholder={"Policy Numnber"}
    underlineColorAndroid="#ff9d00"
    placeholderTextColor="#BABABA"
    value={this.state.mobileno}
    textAlign="left"
    
    onChangeText={(text) => this.setState({mobileno:text})}/>
   </View>

   <TouchableOpacity style={{height:height(6),width:width(45),backgroundColor:"#FFBA00"
   ,borderRadius:24,alignItems:"center",justifyContent:"center"}} onPress={() => Actions.Upload()}>
           <Text style={{color:"#fff",fontWeight:"bold",fontSize:24}}>
              Done
           </Text>
   </TouchableOpacity>
   </ImageBackground>
      </SafeAreaView>
    );
  }
}
