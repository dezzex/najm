import React, { Component } from 'react';
import { View, Text, SafeAreaView,TextInput, ImageBackground, ToastAndroid, ProgressBarAndroid,Image, StatusBar } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
        mobileno:"",
        isloading:true
    };
  }
  componentWillMount = async() => {
    SplashScreen.hide()    

    try {
      const value = await AsyncStorage.getItem('com.dezzex.login')
      console.log("ffd",value);
      
      if(value !== null) {
        // value previously stored
        Actions.Home()
      }
      else{
          this.setState({isloading:false})
      }
    } catch(e) {
      // error reading value
    }
  }
onGoPressed = async () => {
  
  
  if(this.state.mobileno.length > 0){
    await AsyncStorage.setItem('com.dezzex.login', JSON.stringify(this.state.mobileno))
    console.log("ddddddddd",this.state.mobileno.length);
    Actions.otpscreen();

  }
  else{
    ToastAndroid.show("Enter Mobile No..",ToastAndroid.SHORT)
  }
}
  render() {
    return (
      <SafeAreaView style={{width:width(99),alignItems:"center",justifyContent:"center"}}>
        <StatusBar backgroundColor="#2d835e" barStyle="light-content" />
      {!this.state.isloading
      ?
      
      <ImageBackground source={require("../assets/mainbg2.png")} style={{width:width(100),height:height(99),alignItems:"center",justifyContent:"center"}}>
        
        <View style={{width:width(99),height:height(15),alignItems:"center",justifyContent:"center"}}>

        <Image source={require("../assets/logo.png")} resizeMode="contain" style={{width:width(40),height:height(30)}}/>
  
        
        </View>
        <View style={{width:width(99),height:height(30),alignItems:"center",justifyContent:"center"}}>
            <TextInput style={{borderWidth:1.5,borderRadius:24,width:width(75),height:height(6),
                color:"#000",borderColor:"#2d835e",fontSize:18,backgroundColor:"#fff"}}
                placeholder={"+971 Mobile No"}
                placeholderTextColor="#4D4D4D"
                value={this.state.mobileno}
                textAlign="center"
                keyboardType="numeric"
                onChangeText={(text) => this.setState({mobileno:text})}/>
        </View>

        <View style={{width:width(65),height:height(8),marginBottom:height(10),alignItems:"center",justifyContent:"space-between"}}>
                <Text style={{fontSize:22,color:"#696969",fontFamily:"Helvetica Neue"}}>
                    Let's Get Started
                   
                </Text>
                <Text style={{fontSize:22,color:"#696969",fontFamily:"Helvetica Neue"}}>
                Enter your mobile number
                </Text>
        </View>
        <View style={{width:width(65),height:height(20),alignItems:"center",justifyContent:"space-between"}}>
                <Text style={{color:"#696969",fontFamily:"Helvetica Neue"}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </Text>
        </View>
        <TouchableOpacity style={{height:height(7),width:width(65),backgroundColor:"#2d835e"
        ,borderRadius:24,alignItems:"center",justifyContent:"center"}} onPress={this.onGoPressed.bind(this)}>
                <Text style={{color:"#fff",fontWeight:"bold",fontSize:24}}>
                    Let's Go
                </Text>
        </TouchableOpacity>
        </ImageBackground>
        :
        <ImageBackground source={require("../assets/mainbg2.png")} style={{width:width(100),height:height(99),alignItems:"center",justifyContent:"center"}}>
            <ProgressBarAndroid/>
          </ImageBackground>
            }
      </SafeAreaView>
    );
  }
}
