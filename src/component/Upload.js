import React, { Component } from 'react';
import { View, Text, SafeAreaView,ImageBackground,Image,TextInput,TouchableOpacity } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { ScrollView } from 'react-native-gesture-handler';
import StarRating from 'react-native-star-rating';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';

const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
export default class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
        image:''
    };
  }

  onUploadPressed(){
      console.log("hereee")
      var self = this;
      ImagePicker.launchImageLibrary(options, (response) => {
        // Same code as in above section!
        console.log("cdddd",response);
        if(response.data !== undefined){
        self.setState({image:"data:image/png;base64,"+response.data})
        }
      });
  }
  onOpenCamera(){
    ImagePicker.launchCamera(options, (response) => {
        // Same code as in above section!
        console.log("cdddd",response);
        
      });
  }

  render() {
    return (
        <SafeAreaView style={{width:width(99),height:height(99),alignItems:"center",justifyContent:"center"}}>
        
        <View style={{width:width(85),height:height(10),justifyContent:"space-evenly"}}>
        <Text style={{fontSize:32,fontFamily:"Helvetica Neue",color:"#606060",fontWeight:"bold"}}>
        Upload File
        </Text>
        <Text style={{fontSize:14,fontFamily:"Helvetica Neue",color:"#606060",fontWeight:"bold"}}>
        Police Report
        </Text>
     </View>
     <View style={{width:width(85),height:height(35),alignItems:"center",justifyContent:"center"}}>
     {this.state.image.length === 0
     ?
     <Image source={require("../assets/imageicon.png")} resizeMode="contain" style={{width:width(40),height:height(30)}}/>
         :
         <Image source={{uri:this.state.image}} resizeMode="cover" style={{width:width(80),height:height(30)}}/>
      
        }        
     </View>
  
     <View style={{width:width(90),height:height(20),flexDirection:"row",justifyContent:"space-evenly",alignItems:"center"}}>
     <TouchableOpacity style={{height:height(6),width:width(35),borderColor:"#FFBA00",borderWidth:1,
     borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onUploadPressed.bind(this)}>
             <Text style={{color:"#5C5C5C",fontSize:16}}>
                Upload
             </Text>
     </TouchableOpacity>

     <TouchableOpacity style={{height:height(6),width:width(35),borderColor:"#FFBA00",borderWidth:1,
     borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onOpenCamera.bind(this)}>
     <LinearGradient style={{alignItems:"center",justifyContent:"center",height:height(6),width:width(35)
     ,borderRadius:12}}
                 colors={["#FFBA00","#F58400"]} >
             <Text style={{color:"#FFFFFF",fontSize:16}}>
                Take Picture
             </Text>
             </LinearGradient>
     </TouchableOpacity>

     
     </View>
     <View style={{height:height(15),alignItems:"center",justifyContent:"center"}}>
     <TouchableOpacity style={{height:height(6),width:width(45),backgroundColor:"#FFBA00"
   ,borderRadius:24,alignItems:"center",justifyContent:"center"}} onPress={() => Actions.UploadCar()}>
           <Text style={{color:"#fff",fontWeight:"bold",fontSize:24}}>
              Done
           </Text>
   </TouchableOpacity>
     </View>
     </SafeAreaView>
     
    );
  }
}
