import React, { Component } from 'react';
import { View, Text, SafeAreaView,ImageBackground,TextInput,StatusBar } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Actions } from 'react-native-router-flux';

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
        name:'',
        email:'',
        address:''
    };
  }

  render() {
    return (
        <SafeAreaView style={{width:width(99),height:height(99),alignItems:"center",justifyContent:"center"}}>
        
        <StatusBar backgroundColor="#2d835e" barStyle="light-content" />
        <ImageBackground source={require("../assets/mainbg3.png")} resizeMode="contain" style={{width:width(100),height:height(99),alignItems:"center",justifyContent:"center"}}>
      
        <View style={{width:width(85),height:height(75),alignItems:"center",justifyContent:"center"}}>
        <View style={{width:width(85),height:height(4),justifyContent:"center"}}>
            <Text style={{fontSize:16,fontFamily:"Helvetica Neue",color:"#919191"}}>
                Create Account
            </Text>
         </View>

         <View style={{width:width(85),height:height(10),justifyContent:"center"}}>
         <Text style={{fontSize:36,fontFamily:"Helvetica Neue",color:"#464646",fontWeight:"bold"}}>
            Welcome
         </Text>
      </View>

      <View style={{width:width(85),justifyContent:"center"}}>
      <Text style={{fontSize:16,fontFamily:"Helvetica Neue",color:"#646464"}}>
      Lorem ipsum dolor sit amet, consectetur.
      </Text>
   </View>
   <View style={{width:width(85),height:height(40),justifyContent:"center",alignItems:"center"}}>
   <TextInput style={{borderBottomWidth:1,width:width(85),height:height(8),
    color:"#000",borderColor:"#9F9F9F",fontSize:18}}
    placeholder="Name"
    placeholderTextColor="#a8a8a8"
    value={this.state.name}
    
    onChangeText={(text) => this.setState({name:text})}/>
    <TextInput style={{borderBottomWidth:1,width:width(85),height:height(8),
        color:"#000",borderColor:"#9F9F9F",fontSize:18}}
        placeholder="Email Address"
        placeholderTextColor="#a8a8a8"
        value={this.state.email}
        
        onChangeText={(text) => this.setState({email:text})}/>

        <TextInput style={{borderBottomWidth:1,width:width(85),height:height(8),
            color:"#000",borderColor:"#9F9F9F",fontSize:18}}
            placeholder="Address"
            placeholderTextColor="#a8a8a8"
            value={this.state.address}
            
            onChangeText={(text) => this.setState({address:text})}/>
        
</View>
</View>
<TouchableOpacity style={{height:height(7),width:width(65),backgroundColor:"#2d835e"
,borderRadius:16,alignItems:"center",justifyContent:"center",borderRadius:height(7)/2}} onPress={() => Actions.Home()}>
        <Text style={{color:"#fff",fontWeight:"bold",fontSize:24}}>
        Create Account
        </Text>
</TouchableOpacity>
         </ImageBackground>
      </SafeAreaView>
    );
  }
}
