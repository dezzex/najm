import React, { Component } from 'react';
import { View, Text ,SafeAreaView, ActivityIndicator,StatusBar} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import ProgressBar from 'react-native-progress/Bar';
import Result from './Result';
import { Actions } from 'react-native-router-flux';

var bodyFormData = new FormData();
const axios = require('axios').default;

export default class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
        progress:0,
        data:''
    };
  }

  componentDidMount(props){

      var self = this;

     
      if(this.props.data !== undefined && this.props.data.length > 0){
        this.props.data.map((data,idx) => {
          let filename = data.path.substring(data.path.lastIndexOf('/') + 1, data.path.length)
            var photo = {
                uri: data.path,
                type: data.mime,
                name: filename,
              };
            
              
            bodyFormData.append('files[]', photo);
        })
 
    console.log(bodyFormData);

    
      // bodyFormData.append('files[]', this.props.data.data);
      axios({
        method: 'post',
        url: 'http://3.227.99.109:5000/api/upload',
        data: bodyFormData,
        })
        .then(function (response) {
            //handle success
            var data = response.data.data;
            console.log("ffffffff",data,response);
            bodyFormData = new FormData();
            
           if(data !== undefined && data.length > 0){
            // self.setState({data:data})
            self.props.navigation.navigate("Result",{data:data})
           }
        })
        .catch(function (response) {
            //handle error
            console.log(response);
        });
      }

     
  }
  onTimeOut(){
      this.setState({progress:this.state.progress+.5})
      
  }
  render() {
    return (
      <SafeAreaView style={{flex:1,alignItems:"center",justifyContent:"center"}}>
      <StatusBar backgroundColor="#2d835e" barStyle="light-content" />
      <View style={{width:width(99),height:height(90),alignItems:"center",justifyContent:"center"}}>
      <View style={{width:width(85),height:height(10),justifyContent:"space-evenly"}}>
      <Text style={{fontSize:26,fontFamily:"Helvetica Neue",color:"#606060",fontWeight:"bold"}}>
      Assessing damage on car using deep learning...
      </Text>
      </View>
      <View style={{width:width(99),height:height(65),alignItems:"center",justifyContent:"center"}}>
      <ActivityIndicator size="large" color="#2d835e"/>
      </View>
   </View>
   
      </SafeAreaView>
    );
  }
}
