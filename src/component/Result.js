import React, { Component } from 'react';
import { View, Text ,SafeAreaView,StyleSheet,TextInput,TouchableOpacity,Image, ToastAndroid,StatusBar} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';

import { Table, Row, Rows } from 'react-native-table-component';
import { Actions } from 'react-native-router-flux';
import { ScrollView } from 'react-native-gesture-handler';

export default class Result extends Component {
  constructor(props) {
    super(props);
    this.state = {
        tableHead: ['Part','Damage Type','Item Price','Labour Charge'],
        tableData: [
        ],
        list:[],
        comments:'',
        imageuri:'',
        currentIndex:0
    };
  }

  onApprove(){
    ToastAndroid.show("Approved...",ToastAndroid.SHORT);
    Actions.Home()
  }
  onReject(){

  }
  onPostBid(){

  }
  onNextPressed(){
    var index = this.state.currentIndex;
    if(index < this.state.list.length-1){
        index = index + 1;
        this.setState({currentIndex:index})
    }
  }
  onPreviousPressed(){
    var index = this.state.currentIndex;
    if(index > 0){
        index = index - 1;
        this.setState({currentIndex:index})
    }
  }
  componentWillMount(){
    var data = this.props.navigation.getParam('data', '');
    console.log("here",data);
    
    if(data !== undefined){
    
      var data = data;
      var list = [];
      
      if(data !== undefined && data.length > 0){
        data.map((data) => {
          var tableData = [];
          
        if(data.label_text !== undefined && data.label_text.length > 0){
        data.label_text.map((issue) => {

          var list = [];
          var price = issue.price;
          var labcharge = issue.labcharge;

          list.push(issue.part);
          list.push(issue.issue);

          if(price === undefined)
            price = "N/A"
          if(labcharge === undefined)
          labcharge = "N/A"

          list.push(price);
          list.push(labcharge);
          tableData.push(list)
        })
        // list.push(tableData);
        var tableData = tableData;
      }
      var imagesrc = data.aws_path;
      var car = {tableData:tableData,imagesrc:imagesrc}
      list.push(car)
      // setTimeout(this.onImageLoad.bind(this,data.aws_path),500);
        // this.setState({tableData:tableData,imageuri:data.aws_path})
      })
      console.log("Carrrrrrrrrrrrr",list);
      this.setState({list:list})
      
    }
  }
}
onImageLoad(data){
  console.log("Reached hereeeeeee",data);
  this.setState({imageuri:`${data}&random=${Math.random().toString(36).substring(7)}`})
  
}
  render() {
    console.log("In Mount",this.state.list[this.state.currentIndex].imagesrc);
    return (
        <SafeAreaView style={{flex:1,alignItems:"center",justifyContent:"space-evenly"}}>
          
        <StatusBar backgroundColor="#2d835e" barStyle="light-content" />
        <View style={{width:width(99),height:height(90),alignItems:"center",justifyContent:"center"}}>
        <View style={{width:width(85),height:height(10),justifyContent:"space-evenly",alignItems:"center",marginBottom:height(2)}}>
        <Text style={{fontSize:26,fontFamily:"Helvetica Neue",color:"#606060",fontWeight:"bold"}}>
        AI Result
        </Text>
        </View>
        <View style={{width:width(90),height:height(30),marginBottom:height(2)}}>
        <Image onLoad={this.onImageLoad.bind(this)}  source={{uri:this.state.list[this.state.currentIndex].imagesrc + '?' + new Date()}} style={{flex: 1,width: null,height: null,resizeMode: 'stretch'}}/>
        </View>
        <View style={{flexDirection:"row",width:width(99),alignItems:"center",justifyContent:"center",height:height(25)}}>
        <TouchableOpacity onPress={this.onPreviousPressed.bind(this)}>
        <Image  source={require("../assets/left.png")} style={{width:width(10),height:height(10),opacity:this.state.currentIndex === 0 ? 0.10: 1}} resizeMode="contain"/>
        </TouchableOpacity>
        <View style={{height:height(25),width:width(80)}}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.state.list[this.state.currentIndex] !== undefined
            ?
        <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff',backgroundColor:"#fff"}}>
          <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text}/>
          <Rows data={this.state.list[this.state.currentIndex].tableData} textStyle={styles.text}/>
        </Table>
        :
        <Text>
          No Data Found
        </Text>
            }
        </ScrollView>
        </View>
        <TouchableOpacity onPress={this.onNextPressed.bind(this)}>
        <Image  source={require("../assets/right.png")} style={{width:width(10),height:height(10),opacity:this.state.currentIndex === this.state.list.length -1 ? 0.10: 1}} resizeMode="contain"/>
        </TouchableOpacity>
        </View>

        <View style={{width:width(99),height:height(12),alignItems:"center",justifyContent:"space-evenly"}}>
            <Text style={{fontSize:16,fontFamily:"Helvetica Neue",color:"#434343",fontWeight:"bold"}}>
              Kindly pay and drop your car
            </Text>
            <Text style={{fontSize:22,fontFamily:"Helvetica Neue",color:"#434343",fontWeight:"bold"}}>
            Thanks, hope you enjoy the ride
            </Text>
        </View>
        <TouchableOpacity style={{width:width(99),height:height(12),alignItems:"center",justifyContent:"center"}} onPress={() => Actions.Home()}>
        <Image  source={require("../assets/home.png")} style={{width:width(25),height:height(12)}} resizeMode="contain"/>
       
        </TouchableOpacity>

{/*         
        <View style={{width:width(90),height:height(20),justifyContent:"space-evenly",alignItems:"center"}}>
        <View style={{height:height(8),width:width(85),alignItems:"center",justifyContent:"center"}}>
        <TextInput style={{borderWidth:1.5,padding:width(4),borderRadius:12,borderColor:"#2d835e",width:width(85)
        ,height:height(6),
        color:"#000",fontSize:14}}
        placeholder={"Add Comments..."}
        placeholderTextColor="#BABABA"
        value={this.state.comments}
        multiline={true}
        
        onChangeText={(text) => this.setState({comments:text})}/>
        </View>
        <View style={{flexDirection:"row",width:width(99),justifyContent:"space-evenly",alignItems:"center"}}>
        <TouchableOpacity style={{height:height(5),width:width(25),borderColor:"#2d835e",borderWidth:1,backgroundColor:"#2d835e",
        borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onApprove.bind(this)}>
                <Text style={{color:"#FFFFFF",fontSize:16}}>
                   Approve
                </Text>
        </TouchableOpacity>
   
        <TouchableOpacity style={{height:height(5),width:width(25),borderColor:"#2d835e",borderWidth:1,backgroundColor:"#2d835e",
        borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onPostBid.bind(this)}>
        
                <Text style={{color:"#FFFFFF",fontSize:16}}>
                   Post For Bid
                </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{height:height(5),width:width(25),borderColor:"#E64700",borderWidth:1,backgroundColor:"#fff",
        borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onReject.bind(this)}>
        
                <Text style={{color:"#E64700",fontSize:16}}>
                   Reject
                </Text>
        </TouchableOpacity>
        </View>
        </View> */}
       
        {/* <View style={{width:width(85),height:height(60),justifyContent:"space-evenly",alignItems:"center"}}>
          <View style={{width:width(99),height:height(25),alignItems:"center",justifyContent:"center",marginBottom:height(5)}}>
        <Image source={{uri:this.state.imageuri}} style={{width:width(99),height:height(20)}} resizeMode="contain"/>
        </View>
        <View style={{height:height(30),width:width(90)}}>
          <ScrollView>
        <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff',backgroundColor:"#fff"}}>
          <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text}/>
          <Rows data={this.state.tableData} textStyle={styles.text}/>
        </Table>
        </ScrollView>
        </View>
        <View style={{height:height(20),width:width(85),alignItems:"center",justifyContent:"center",marginBottom:height(5)}}>
        <TextInput style={{borderWidth:1.5,padding:width(4),borderRadius:12,borderColor:"#2d835e",width:width(85),height:height(25),
        color:"#000",fontSize:18}}
        placeholder={"Add Comments..."}
        placeholderTextColor="#BABABA"
        value={this.state.comments}
        multiline={true}
        
        onChangeText={(text) => this.setState({comments:text})}/>
        </View>
        </View> */}
        {/* <View style={{width:width(90),height:height(20),flexDirection:"row",justifyContent:"space-evenly",alignItems:"center"}}>
        
        <TouchableOpacity style={{height:height(5),width:width(25),borderColor:"#2d835e",borderWidth:1,backgroundColor:"#2d835e",
        borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onApprove.bind(this)}>
                <Text style={{color:"#FFFFFF",fontSize:16}}>
                   Approve
                </Text>
        </TouchableOpacity>
   
        <TouchableOpacity style={{height:height(5),width:width(25),borderColor:"#2d835e",borderWidth:1,backgroundColor:"#2d835e",
        borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onPostBid.bind(this)}>
        
                <Text style={{color:"#FFFFFF",fontSize:16}}>
                   Post For Bid
                </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{height:height(5),width:width(25),borderColor:"#E64700",borderWidth:1,backgroundColor:"#fff",
        borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onReject.bind(this)}>
        
                <Text style={{color:"#E64700",fontSize:16}}>
                   Reject
                </Text>
        </TouchableOpacity>
        
        </View> */}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
    head: { height: 40 },
    text: { margin: 6 }
  });