import React, { Component } from 'react';
import { View, Text, SafeAreaView,ImageBackground,Image,TextInput,TouchableOpacity, ToastAndroid,StatusBar } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { ScrollView } from 'react-native-gesture-handler';
import StarRating from 'react-native-star-rating';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
// import ImagePicker from 'react-native-image-picker';
import { log } from 'react-native-reanimated';
// import ImagePicker from "react-native-customized-image-picker";
import ImagePicker from 'react-native-image-crop-picker';

const options = {
    title: 'Select Avatar',
    multiple:true,
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
export default class UploadCar extends Component {
  constructor(props) {
    super(props);
    this.state = {
        image:[undefined,undefined,undefined,undefined],
        data:[0,1,2,3],
        list:[0,1,2,3],
        index:0
    };
  }

  onUploadPressed(index){
      console.log("hereee")
      var self = this;
      ImagePicker.openPicker({
        includeBase64:true,
        multiple:true,
        mediaType: "photo",
      }).then(response => {
console.log(response);

           if(response !== undefined){
          
        var imagelist = this.state.image;
        var data = this.state.data;
        response.map((photo,index) => {
          if(index === 0)
            index = this.state.index;

          imagelist.splice(index,1,"data:image/png;base64,"+photo.data);
          data.splice(index,1,photo)
        })
        
        console.log(imagelist,data);
        
        self.setState({image:imagelist,data:data})
        }
      });
      // ImagePicker.launchImageLibrary(options, (response) => {
      //   // Same code as in above section!
      //   if(response.data !== undefined){
      //   var imagelist = this.state.image;
      //   var data = this.state.data;
      //   imagelist.splice(this.state.index,1,"data:image/png;base64,"+response.data);
      //   data.splice(this.state.index,1,response)
        
      //   self.setState({image:imagelist,data:data})
      //   }
      // });
    
  }
  onOpenCamera(){
    var self = this;
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      includeBase64: true,
    }).then(response => {
            console.log("ccccc",response);
                    if(response.data !== undefined){
          var imagelist = this.state.image;
          var data = this.state.data;
          imagelist.splice(this.state.index,1,"data:image/png;base64,"+response.data);
          data.splice(this.state.index,1,response)
          
          self.setState({image:imagelist,data:data})
          }
    });
    // ImagePicker.openPicker( (response) => {
    //     console.log("cdddd",response);
    //     if(response.data !== undefined){
    //       var imagelist = this.state.image;
    //       var data = this.state.data;
    //       imagelist.splice(this.state.index,1,"data:image/png;base64,"+response.data);
    //       data.splice(this.state.index,1,response)
          
    //       self.setState({image:imagelist,data:data})
    //       }
    //   });
  }
  onImageIconPressed(index){
    this.setState({index:index})
  }
  onFinalUploadPressed(){
    var data = [];
    

    this.state.data.map((photo)=>{
      if(photo.height !== undefined)
        data.push(photo);
    })
 
    
    if(data.length > 0){
      console.log(data.length);
      Actions.Loading({data:data})
    }
    else
      return(ToastAndroid.show("Select an image..",ToastAndroid.SHORT))
  }

  render() {
    
    return (
        <SafeAreaView style={{width:width(99),height:height(99),alignItems:"center",justifyContent:"center"}}>
        
        <StatusBar backgroundColor="#2d835e" barStyle="light-content" />
        <View style={{width:width(85),height:height(10),alignItems:"center",justifyContent:"space-evenly"}}>
        <Text style={{fontSize:26,fontFamily:"Helvetica Neue",color:"#606060",fontWeight:"bold"}}>
        Upload
        </Text>
        <Text style={{fontSize:18,fontFamily:"Helvetica Neue",color:"#606060",fontWeight:"bold"}}>
        images of Car
        </Text>
     </View>
   <View  style={{width:width(99),height:height(35),alignItems:"center",justifyContent:"center",marginBottom:height(5)}} >
   {this.state.image[this.state.index] === undefined
   ?
   <Image source={require("../assets/imageicon.png")} resizeMode="contain" style={{width:width(40),height:height(30)}}/>
       :
       <Image source={{uri:this.state.image[this.state.index]}} resizeMode="cover" style={{width:width(80),height:height(30)}}/>
    }
         
   </View>
     <View style={{width:width(99),height:height(15),flexDirection:"row",justifyContent:"space-evenly",alignItems:"center"}}>
     <TouchableOpacity style={{height:height(13),width:width(20),borderColor:"#2d835e",borderWidth:this.state.index === 0 ? 5 : 1,
     borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onImageIconPressed.bind(this,0)}>
      
        {this.state.image[0] === undefined
        ?
             <Text style={{color:"#5C5C5C",fontSize:16}}>
                Upload
             </Text>
             :
             <View>
               
             <Image source={{uri:this.state.image[0]}} resizeMode="cover" style={{width:width(18),height:height(11)}}/>
             </View>
        }
     </TouchableOpacity>
     <TouchableOpacity style={{height:height(13),width:width(20),borderColor:"#2d835e",borderWidth:this.state.index === 1 ? 5 : 1,
     borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onImageIconPressed.bind(this,1)}>
                  {this.state.image[1] === undefined
        ?
             <Text style={{color:"#5C5C5C",fontSize:16}}>
                Upload
             </Text>
             :
             <Image source={{uri:this.state.image[1]}} resizeMode="cover" style={{width:width(18),height:height(11)}}/>
    
        }
     </TouchableOpacity>
     <TouchableOpacity style={{height:height(13),width:width(20),borderColor:"#2d835e",borderWidth:this.state.index === 2 ? 5 : 1,
     borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onImageIconPressed.bind(this,2)}>
                   {this.state.image[2] === undefined
        ?
             <Text style={{color:"#5C5C5C",fontSize:16}}>
                Upload
             </Text>
             :
             <Image source={{uri:this.state.image[2]}} resizeMode="cover" style={{width:width(18),height:height(11)}}/>
    
        }
     </TouchableOpacity>
     <TouchableOpacity style={{height:height(13),width:width(20),borderColor:"#2d835e",borderWidth:this.state.index === 3 ? 5 : 1,
     borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onImageIconPressed.bind(this,3)}>
                  {this.state.image[3] === undefined
        ?
             <Text style={{color:"#5C5C5C",fontSize:16}}>
                Upload
             </Text>
             :
             <View>
              
             <Image source={{uri:this.state.image[3]}} resizeMode="cover" style={{width:width(18),height:height(11)}}/>
             </View>
    
        }
     </TouchableOpacity>
     </View>
     <View style={{width:width(90),height:height(20),flexDirection:"row",justifyContent:"space-evenly",alignItems:"center"}}>
     <TouchableOpacity style={{height:height(6),width:width(35),borderColor:"#2d835e",borderWidth:1,
     borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onOpenCamera.bind(this)}>
             <Text style={{color:"#5C5C5C",fontSize:16}}>
                Take Photo
             </Text>
     </TouchableOpacity>

     <TouchableOpacity style={{height:height(6),width:width(35),borderColor:"#2d835e",borderWidth:1,
     borderRadius:12,alignItems:"center",justifyContent:"center"}} onPress={this.onUploadPressed.bind(this)}>
     <LinearGradient style={{alignItems:"center",justifyContent:"center",height:height(6),width:width(35)
     ,borderRadius:12}}
                 colors={["#2d835e","#2d835e"]} >
             <Text style={{color:"#FFFFFF",fontSize:16}}>
              From Gallery
             </Text>
             </LinearGradient>
     </TouchableOpacity>

     
     </View>
     <View style={{height:height(15),alignItems:"center",justifyContent:"center"}}>
     <TouchableOpacity style={{height:height(6),width:width(45),backgroundColor:"#2d835e"
   ,borderRadius:24,alignItems:"center",justifyContent:"center"}} onPress={this.onFinalUploadPressed.bind(this)}>
           <Text style={{color:"#fff",fontWeight:"bold",fontSize:24}}>
              Upload
           </Text>
   </TouchableOpacity>
     </View>
     </SafeAreaView>
     
    );
  }
}
