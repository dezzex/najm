import React, { Component } from 'react';
import { View, Text, SafeAreaView,ImageBackground,Image,StatusBar } from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import StarRating from 'react-native-star-rating';

import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
        starCount:4
    };
  }

  render() {
    return (
        <SafeAreaView style={{flex:1,width:width(100),height:height(99),alignItems:"center",justifyContent:"center"}}>
          
        <StatusBar backgroundColor="#2d835e" barStyle="light-content" />
        <ImageBackground source={require("../assets/mainbg4.png")} resizeMode="contain" style={{width:width(100),height:height(99),alignItems:"center",justifyContent:"center"}}>
       <View style={{flexDirection:"row",width:width(99),height:height(50),alignItems:"center",justifyContent:"space-evenly"}}>
        <TouchableOpacity style={{height:height(25),alignItems:"center",justifyContent:"center"}} onPress={() => Actions.UploadCar()}>
     <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(34),height:height(22)
     ,borderRadius:24}}
                 colors={["#2d835e","#2d835e"]} >
                 <Image source={require("../assets/pickup.png")} resizeMode="contain" style={{width:width(15),height:height(15)}}/>
                    <Text style={{color:"#fff",fontSize:18}}>
                       Pick Up Car
                    </Text>
                </LinearGradient>
    </TouchableOpacity>
    <TouchableOpacity style={{height:height(25),alignItems:"center",justifyContent:"center"}} onPress={() => Actions.UploadCar()}>
     <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(34),height:height(22)
     ,borderRadius:24}}
                 colors={["#2d835e","#2d835e"]} >
                 <Image source={require("../assets/drop.png")} resizeMode="contain" style={{width:width(15),height:height(15)}}/>
                    <Text style={{color:"#fff",fontSize:18}}>
                       Drop Car
                    </Text>
                </LinearGradient>
    </TouchableOpacity>
    </View>
    <Image source={require("../assets/mini.png")} resizeMode="contain" style={{width:width(95),height:height(45)}}/>
                
        {/* <View style={{width:width(85),height:height(10),alignItems:"center",justifyContent:"center"}}>
        <Text style={{fontSize:28,fontFamily:"Helvetica Neue",color:"#4D4D4D",fontWeight:"bold"}}>
        Let's start the claim
        </Text>
     </View>

     <TouchableOpacity style={{height:height(25),alignItems:"center",justifyContent:"center"}} onPress={() => Actions.PolicyDetails()}>
     <LinearGradient style={{alignItems:"center",justifyContent:"center",width:width(34),height:height(22)
     ,borderRadius:24}}
                 colors={["#0073ff","#2d835e"]} >
                 <Image source={require("../assets/fileclaim.png")} resizeMode="contain" style={{width:width(10),height:height(10)}}/>
                    <Text style={{color:"#fff",fontSize:16}}>
                       FILES CLAIM
                    </Text>
                </LinearGradient>
    </TouchableOpacity>

    <View style={{height:height(25),alignItems:"center",justifyContent:"center"}}>
    <Text style={{fontSize:26,fontFamily:"Helvetica Neue",color:"#4D4D4D"}}>
    Near by Service centres
    </Text>
    </View>

    <View style={{height:height(20)}}> */}
    {/* <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity style={{width:width(85),height:height(10),marginBottom:height(2),borderWidth:1.5,backgroundColor:"#fff",borderColor:"#FFBA00",
                borderRadius:25,alignItems:"center",justifyContent:"center"}}>
            <View style={{flexDirection:"row",height:height(5)}}>
            <View style={{width:width(60),justifyContent:"center"}}>
            <Text>
            New Service Center
            </Text>
            </View>
            <View style={{width:width(20),justifyContent:"center"}}>
            <Text>
            200 meters away
            </Text>
            </View>
            
        </View>
        <View style={{width:width(80)}}>
        <View style={{width:width(20)}}>
        <StarRating
        disabled={false}
        maxStars={5}
        rating={this.state.starCount}
        starSize={height(2)}
        fullStarColor={"#FFC94E"}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
      />
      </View>
      </View>
           
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(85),height:height(10),marginBottom:height(2),borderWidth:1.5,backgroundColor:"#fff",borderColor:"#FFBA00",
                borderRadius:25,alignItems:"center",justifyContent:"center"}}>
            <View style={{flexDirection:"row",height:height(5)}}>
            <View style={{width:width(60),justifyContent:"center"}}>
            <Text>
            New Service Center
            </Text>
            </View>
            <View style={{width:width(20),justifyContent:"center"}}>
            <Text>
            200 meters away
            </Text>
            </View>
            
        </View>
        <View style={{width:width(80)}}>
        <View style={{width:width(20)}}>
        <StarRating
        disabled={false}
        maxStars={5}
        rating={this.state.starCount}
        starSize={height(2)}
        fullStarColor={"#FFC94E"}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
      />
      </View>
      </View>
           
        </TouchableOpacity>
        <TouchableOpacity style={{width:width(85),height:height(10),borderWidth:1.5,backgroundColor:"#fff",borderColor:"#FFBA00",
        borderRadius:25,alignItems:"center",justifyContent:"center"}}>
    <View style={{flexDirection:"row",height:height(5)}}>
    <View style={{width:width(60),justifyContent:"center"}}>
    <Text>
    New Service Center
    </Text>
    </View>
    <View style={{width:width(20),justifyContent:"center"}}>
    <Text>
    200 meters away
    </Text>
    </View>
    
</View>
<View style={{width:width(80)}}>
<View style={{width:width(20)}}>
<StarRating
disabled={false}
maxStars={5}
rating={this.state.starCount}
starSize={height(2)}
fullStarColor={"#FFC94E"}
selectedStar={(rating) => this.onStarRatingPress(rating)}
/>
</View>
</View>
   
</TouchableOpacity>

        </ScrollView>
        </View> */}
      </ImageBackground>
      </SafeAreaView>
    );
  }
}
