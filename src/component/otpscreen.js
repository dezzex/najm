import React, { Component } from 'react';
import { View, Text, SafeAreaView,ImageBackground,TextInput ,Image,StatusBar} from 'react-native';
import { width, height, totalSize } from 'react-native-dimension';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/AntDesign';

export default class otpscreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
        otpfirst:"",
        otpsecond:"",
        otpthird:"",
        otpfourth:"",
    };
  }

  render() {
    return (
      <SafeAreaView>
        
        <StatusBar backgroundColor="#2d835e" barStyle="light-content" />
      <ImageBackground source={require("../assets/mainbg2.png")} style={{width:width(100),height:height(99),alignItems:"center",justifyContent:"center"}}>
      <View style={{width:width(99),height:height(35),alignItems:"center",justifyContent:"center"}}>
      <Image source={require("../assets/logo.png")} resizeMode="contain" style={{width:width(40),height:height(30)}}/>
  </View>
  <View style={{width:width(99),height:height(8),alignItems:"center",justifyContent:"center"}}>

        <Text style={{fontSize:20,fontFamily:"Helvetica Neue",color:"#5A5A5A"}}>
        OTP has been sent to your
        </Text>
        <Text style={{fontSize:20,fontFamily:"Helvetica Neue",color:"#5A5A5A"}}>
        mobile number
        </Text>

</View>
  <View style={{width:width(75),height:height(30),alignItems:"center",justifyContent:"space-between",flexDirection:"row"}}>
      <TextInput style={{borderWidth:1.5,borderRadius:24,width:width(16),height:height(8),
          color:"#000",borderColor:"#2d835e",fontSize:18,backgroundColor:"#fff"}}
          placeholder={"0"}
          placeholderTextColor="#4D4D4D"
          value={this.state.otpfirst}
          textAlign="center"
          maxLength={1}
          keyboardType={'numeric'}
          
          onChangeText={(text) => this.setState({otpfirst:text})}/>

          <TextInput style={{borderWidth:1.5,borderRadius:24,width:width(16),height:height(8),
            color:"#000",borderColor:"#2d835e",fontSize:18,backgroundColor:"#fff"}}
            placeholder={"0"}
            placeholderTextColor="#4D4D4D"
            value={this.state.otpsecond}
            textAlign="center"  
            maxLength={1}
            keyboardType={'numeric'}
            
            onChangeText={(text) => this.setState({otpsecond:text})}/>
            <TextInput style={{borderWidth:1.5,borderRadius:24,width:width(16),height:height(8),
                color:"#000",borderColor:"#2d835e",fontSize:18,backgroundColor:"#fff"}}
                placeholder={"0"}
                placeholderTextColor="#4D4D4D"
                value={this.state.otpthird}
                textAlign="center"
                maxLength={1}
                keyboardType={'numeric'}
                
                onChangeText={(text) => this.setState({otpthird:text})}/>
                <TextInput style={{borderWidth:1.5,borderRadius:24,width:width(16),height:height(8),
                    color:"#000",borderColor:"#2d835e",fontSize:18,backgroundColor:"#fff"}}
                    placeholder={"0"}
                    placeholderTextColor="#4D4D4D"
                    value={this.state.otpfourth}
                    textAlign="center"
                    maxLength={1}
                    keyboardType={'numeric'}
                    
                    onChangeText={(text) => this.setState({otpfourth:text})}/>
  </View>
  <TouchableOpacity style={{height:height(7),width:width(65),borderRadius:24,alignItems:"center",justifyContent:"center"}} onPress={() => Actions.SignUp()}>
          <Text style={{color:"#696969",fontWeight:"bold",fontSize:24}}>
              Let's Get In
          </Text>
  </TouchableOpacity>
  <View style={{width:width(99),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
                    <Text style={{color:"#454545"}}>
                    Didn’t receive the top?
                    </Text>
  </View>
  <View style={{width:width(99),height:height(8),alignItems:"center",justifyContent:"center",flexDirection:"row"}}>
  <TouchableOpacity>
  <Text style={{color:"#454545",fontSize:18}}>
                        Resend
  </Text>
  </TouchableOpacity>
</View>
      </ImageBackground>
      </SafeAreaView>
    );
  }
}
